﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    private BoardController boardController;
    private List<Enemy> enemies;

    internal void AddEnemyToList(Dragon dragon)
    {
        throw new NotImplementedException();
    }

    private GameObject levelImage;
    private Text levelText;
    private GameObject startScreen;
    private Text startText;
    private GameObject levelSelect;
    private Button travelerButton;
    private Button archerButton;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int LevelStartTime = 2;
    private int currentLevel = 1;//1


  /*public void PlayTraveler()
    {
        Button travelerButton = GetComponent("Traveler Button") as Button;
       travelerButton.onClick(StartCoroutine(GameController.Start));
       
        //base.Start();
        Application.LoadLevel(Application.loadedLevel);
    }*/



        void Awake () {
        if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}

    void Start()
    {
        Application.LoadLevel(Application.loadedLevel);
        currentLevel = 0;
        InitializeGame();//game
    }

    private void InitializeStartScreen()
    {
        startScreen = GameObject.Find("Start Screen");
        startText = GameObject.Find("Start Text").GetComponent<Text>();
        startText.text = "Welcome";
        startScreen.SetActive(true);
       // Invoke("DisableStartScreen", LevelStartTime);
    }

    private void InitializeGame()
    {

        if(startScreen != null)
        {
            startScreen.SetActive(false);

        }

        else
        {
            startScreen = GameObject.Find("Start Screen");
            startScreen.SetActive(true);
        }
        startScreen.SetActive(false);

        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        settingUpGame = true;
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
        
    }



    private void DisableStartScreen()
    {
        startScreen.SetActive(false);
        InitializeGame();

    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;

    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
         currentLevel++;
         InitializeGame();
    }

	void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " days...";
        levelImage.SetActive(true);
        enabled = false;

    }
}
